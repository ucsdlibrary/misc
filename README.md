# misc

This repo is a storage bin for issues that are not directly related to a single piece of software, or for platform issues and dialogs that only intersect our software development in tangential ways.

There shouldn't be any code or files other than this readme in here, since repos already exist to collect code snippets for various aspects of our devops work.